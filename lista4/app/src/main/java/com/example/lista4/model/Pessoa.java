package com.example.lista4.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Pessoa implements Serializable {
    int idade;
    double altura;
    EOlhos olhos;
    ECabelos cabelos;
    ESexo sexo;

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public EOlhos getOlhos() {
        return olhos;
    }

    public void setOlhos(EOlhos olhos) {
        this.olhos = olhos;
    }

    public ECabelos getCabelos() {
        return cabelos;
    }

    public void setCabelos(ECabelos cabelos) {
        this.cabelos = cabelos;
    }

    public ESexo getSexo() {
        return sexo;
    }

    public void setSexo(ESexo sexo) {
        this.sexo = sexo;
    }

    public Pessoa(int idade, double altura, EOlhos olhos, ECabelos cabelos, ESexo sexo) {
        this.idade = idade;
        this.altura = altura;
        this.olhos = olhos;
        this.cabelos = cabelos;
        this.sexo = sexo;
    }

    public Pessoa() {
    }
}
