package com.example.lista4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lista4.model.ECabelos;
import com.example.lista4.model.EOlhos;
import com.example.lista4.model.ESexo;
import com.example.lista4.model.Pessoa;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextInputLayout cpIdade, cpAltura;
    RadioButton cpMasc, cpFem;
    RadioGroup cpSexo;
    Button btSalvar, btMostrar;
    Spinner corCabelos, corOlhos;
    Toast toast;
    ArrayList<Pessoa> pessoas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding();

        preencheSpinnerCabelos();
        preencheSpinnerOlhos();

        pessoas = new ArrayList<>();

        btSalvar.setOnClickListener(salvarClick());
        btMostrar.setOnClickListener(mostrarClick());
    }

    private View.OnClickListener mostrarClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double maiorAltura = 0, menorAltura = 10000;
                double contaMulheres = 0;
                double contaHomens = 0;
                double contaLoiraOlhosClaros = 0;
                double mediaMulheres = 0, sum = 0;
                double totalPessoas = 0;


                for (int i = 0; i < pessoas.size(); i++) {
                    if (pessoas.get(i).getAltura() > maiorAltura) {
                        maiorAltura = pessoas.get(i).getAltura();
                    }
                    if (pessoas.get(i).getAltura() < menorAltura) {
                        menorAltura = pessoas.get(i).getAltura();
                    }
                    if (pessoas.get(i).getSexo() == ESexo.Masculino) {
                        contaHomens++;
                    }
                    if (pessoas.get(i).getSexo() == ESexo.Feminino) {
                        contaMulheres++;
                        sum = sum + pessoas.get(i).getAltura();
                        mediaMulheres = sum/contaMulheres;
                    }
                    if (pessoas.get(i).getSexo() == ESexo.Feminino
                            && pessoas.get(i).getOlhos() == EOlhos.Verdes
                            && pessoas.get(i).getCabelos() == ECabelos.Louros
                            && pessoas.get(i).getIdade() >= 18
                            && pessoas.get(i).getIdade() <= 35) {
                        contaLoiraOlhosClaros++;
                    }

                    totalPessoas = contaHomens+contaMulheres;


                }


                double pctgM = contaMulheres/totalPessoas*100;
                double pctgH = contaHomens/totalPessoas*100;
                double pctgLV = contaLoiraOlhosClaros/totalPessoas*100;

                showToast("a) A maior altura é: "+maiorAltura+" A menor altura é: "+menorAltura+
                        "\n"+
                        "b) A média de altura das mulheres é: "+mediaMulheres+
                        "\n"+
                        "c) O número de homens é: "+(int)Math.round(contaHomens)+
                        "\n"+
                        "d) A porcentagem de Homens/Mulheres é: "+pctgH+"%/"+pctgM+"%"+
                        "\n"+
                        "e) A porcentagem de mulheres entre 18-35, loiras de olhos verdes é: "+pctgLV+"%."
                );
            }
        };
    }

    private View.OnClickListener salvarClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                corOlhos.getSelectedItemPosition();

                pessoas.add (new Pessoa (Integer.parseInt(cpIdade.getEditText().getText().toString()),
                        Double.parseDouble(cpAltura.getEditText().getText().toString()),
                        EOlhos.valueOf(corOlhos.getSelectedItem().toString()),
                        ECabelos.valueOf(corCabelos.getSelectedItem().toString()),
                        cpMasc.isChecked()? ESexo.Masculino : ESexo.Feminino));


                showToast("Pessoa salva.");


            }
        };

    }

    private void binding() {

        cpIdade = findViewById(R.id.tilIdade);
        cpAltura = findViewById(R.id.tilAltura);
        cpMasc = findViewById(R.id.rMasc);
        btSalvar = findViewById(R.id.btnSalvar);
        btMostrar = findViewById(R.id.btnMostrar);
        corOlhos = findViewById(R.id.spOlhos);
        corCabelos = findViewById(R.id.spCabelos);
    }

    private void preencheSpinnerCabelos() {

        corCabelos.setAdapter(new ArrayAdapter<ECabelos>(this, android.R.layout.simple_spinner_item, ECabelos.values()));
    }

    private void preencheSpinnerOlhos() {

        corOlhos.setAdapter(new ArrayAdapter<EOlhos>(this, android.R.layout.simple_spinner_item, EOlhos.values()));
    }


    public void showToast(String message) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }
}