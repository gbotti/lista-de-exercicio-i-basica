package com.example.lista2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.lista2.model.Pessoa;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    Button btnVerify;
    TextInputLayout cpIdade, cpAnosTrabalhados;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding();

        btnVerify.setOnClickListener( verifyClick() );
    }

    private View.OnClickListener verifyClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Pessoa p = new Pessoa (  Integer.parseInt(cpIdade.getEditText().getText().toString()),
                        Integer.parseInt(cpAnosTrabalhados.getEditText().getText().toString())
                );

                if (p.getIdade()>65 || p.getAnosTrabalhados() > 30 || p.getIdade()>60 && p.getAnosTrabalhados()>25){
                    Toast.makeText(getApplicationContext(), "Está qualificado para aposentadoria. ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Não está qualificado para aposentadoria. ", Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    private void binding() {
        cpIdade = findViewById(R.id.tilIdade);
        cpAnosTrabalhados = findViewById(R.id.tilAnosTrabalhados);
        btnVerify = findViewById(R.id.btnVerificar);
    }
}