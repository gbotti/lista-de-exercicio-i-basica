package com.example.lista2.model;

import java.io.Serializable;

public class Pessoa  {
    private int idade, anosTrabalhados;

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getAnosTrabalhados() {
        return anosTrabalhados;
    }

    public void setAnosTrabalhados(int anosTrabalhados) {
        this.anosTrabalhados = anosTrabalhados;
    }

    public Pessoa(int idade, int anosTrabalhados) {
        this.idade = idade;
        this.anosTrabalhados = anosTrabalhados;
    }

    public Pessoa() {
    }
}
