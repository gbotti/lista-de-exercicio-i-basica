package com.example.lista3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    TextInputLayout cpHoras, cpSalarioHr, cpDependentes;
    Button btSalvar;
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding();

        btSalvar.setOnClickListener(salvarClick());
    }

    private View.OnClickListener salvarClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                double descINSS = 0, descIR = 0, salarioLiq = 0;
                double salarioBruto = (Integer.parseInt(cpHoras.getEditText().getText().toString()) * Double.parseDouble(cpSalarioHr.getEditText().getText().toString())) + 
                        (50 * Integer.parseInt(cpDependentes.getEditText().getText().toString()));
                
                if (salarioBruto <= 1000) {
                    descINSS = salarioBruto * 8.5 / 100;
                } else if (salarioBruto > 1000) {
                    descINSS = salarioBruto * 9 / 100;
                }

                if (salarioBruto > 500 && salarioBruto <= 1000) {
                    descIR = salarioBruto * 5 / 100;
                } else if (salarioBruto > 1000) {
                    descIR = salarioBruto * 7 / 100;
                }

                salarioLiq = salarioBruto-descINSS-descIR;

                showToast("O salário bruto é de: R$"+salarioBruto+
                        "\n"+
                        "O desconto INSS é de: R$"+descINSS+
                        "\n"+
                        "O desconto IR é de: R$"+descIR+
                        "\n"+
                        "O salário líquido é de: R$"+salarioLiq+
                        "\n"
                );
            }
        };
    }

    private void binding() {

        cpHoras = findViewById(R.id.tilHoras);
        cpSalarioHr = findViewById(R.id.tilSalarioHora);
        cpDependentes = findViewById(R.id.tilDependentes);
        btSalvar = findViewById(R.id.button);

    }

    public void showToast(String message) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }
}