package com.example.lista6.model;

public class Pessoa {

    private String nome;
    private boolean votou;
    private int votos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return nome;
    }

    public boolean isVotou() {
        return votou;
    }

    public void setVotou(boolean votou) {
        this.votou = votou;
    }

    public int getVotos() {
        return votos;
    }

    public void setVotos(int votos) {
        this.votos = votos;
    }

    public Pessoa(String nome, boolean votou, int votos) {
        this.nome = nome;
        this.votou = votou;
        this.votos = votos;
    }

    public Pessoa() {
    }
}

