package com.example.lista6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lista6.model.Pessoa;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    Spinner cpVotar;
    TextView lblInfo;
    Button btnSalvar;
    Button btnVencedor;
    int i, votaram = 0;
    Toast toast;


    private ArrayList<Pessoa> pessoas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding();

        pessoas = new ArrayList<>();
        pessoas.add (new Pessoa("Doguinho Caramelo", false, 0));
        pessoas.add (new Pessoa("Jao", false, 0));
        pessoas.add (new Pessoa("Ze", false, 0));
        pessoas.add (new Pessoa("Gustavo", false, 0));
        pessoas.add (new Pessoa("Daves", false, 0));
        pessoas.add (new Pessoa("Ana", false, 0));
        pessoas.add (new Pessoa("Maria", false, 0));
        pessoas.add (new Pessoa("Um gato", false, 0));
        pessoas.add (new Pessoa("Kurt Cobain", false, 0));
        pessoas.add (new Pessoa("Darth Vader", false, 0));

        lblInfo.setText(pessoas.get(i)+ " realize seu voto");

        preencheSpinner(pessoas);

        btnSalvar.setOnClickListener( computarVoto() );
        btnVencedor.setOnClickListener( computarVencedor() );
    }

    private View.OnClickListener computarVencedor() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int maiorVoto = 0;
                String vencedor = "";
                int counter = 0;

                for (int y = 0; y < pessoas.size(); y++){
                    if (pessoas.get(y).getVotos() > maiorVoto){
                        maiorVoto =  pessoas.get(y).getVotos();
                        vencedor =  pessoas.get(y).getNome();
                        counter = 0;
                    } else if (pessoas.get(y).getVotos() == maiorVoto
                            && pessoas.get(y).getVotos() > 0)
                    {
                        counter++;
                    }

                }

                if (votaram == pessoas.size() && counter > 0){
                    showToast("Houve um empate! Refaça a votação para encontrar um vencedor");
                }else if (votaram == pessoas.size()){
                    showToast("O vencedor é: "+vencedor+" com "+maiorVoto+" votos");
                }else if (votaram < pessoas.size()){
                    showToast("Falta alguem votar...");
                }

            }
        };
    }

    private View.OnClickListener computarVoto() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pessoas.get(i).isVotou() == true){
                    showToast("Essa pessoa já votou. ");
                    if(i == pessoas.size()-1){
                        i = 0;
                    } else {
                        i++;
                    };
                } else {
                    Pessoa p = pessoas.get( cpVotar.getSelectedItemPosition());
                    p.setVotos((p.getVotos()+1));
                    showToast("Voto computado. ");
                    pessoas.get(i).setVotou(true); votaram++;
                    if(i == pessoas.size()-1){
                        i = 0;
                    } else {
                        i++;
                    };

                }
                lblInfo.setText(pessoas.get(i).getNome()+ " realize seu voto");

            }
        };
    }

    private void preencheSpinner(ArrayList<Pessoa> pessoas) {
        ArrayList<String> lista = new ArrayList<>();
        for (Pessoa p : pessoas) {
            lista.add(p.getNome());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_1, lista);

        ArrayAdapter<Pessoa> adapter1 = new ArrayAdapter<Pessoa>(getApplicationContext(),
                android.R.layout.simple_list_item_1, pessoas);

        cpVotar.setAdapter(adapter1);
    }

    private void binding() {
        cpVotar = findViewById(R.id.spVotar);
        lblInfo = findViewById(R.id.tvInfo);
        btnSalvar = findViewById(R.id.button);
        btnVencedor = findViewById(R.id.button2);
    }

    public void showToast(String message) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }
}