package com.example.lista1.model;

public class Carro {
    EMotor motor;
    EPintura pintura;
    boolean ar, cambio, vidro, alarme, solar, mult;

    public EMotor getMotor() {
        return motor;
    }

    public void setMotor(EMotor motor) {
        this.motor = motor;
    }

    public EPintura getPintura() {
        return pintura;
    }

    public void setPintura(EPintura pintura) {
        this.pintura = pintura;
    }

    public boolean isAr() {
        return ar;
    }

    public void setAr(boolean ar) {
        this.ar = ar;
    }

    public boolean isCambio() {
        return cambio;
    }

    public void setCambio(boolean cambio) {
        this.cambio = cambio;
    }

    public boolean isVidro() {
        return vidro;
    }

    public void setVidro(boolean vidro) {
        this.vidro = vidro;
    }

    public boolean isAlarme() {
        return alarme;
    }

    public void setAlarme(boolean alarme) {
        this.alarme = alarme;
    }

    public boolean isSolar() {
        return solar;
    }

    public void setSolar(boolean solar) {
        this.solar = solar;
    }

    public boolean isMult() {
        return mult;
    }

    public void setMult(boolean mult) {
        this.mult = mult;
    }

    public Carro() {
    }

    public Carro(EMotor motor, EPintura pintura, boolean ar, boolean cambio, boolean vidro, boolean alarme, boolean solar, boolean mult) {
        this.motor = motor;
        this.pintura = pintura;
        this.ar = ar;
        this.cambio = cambio;
        this.vidro = vidro;
        this.alarme = alarme;
        this.solar = solar;
        this.mult = mult;
    }
}
