package com.example.lista1;

import static com.example.lista1.model.EPintura.Nenhuma;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lista1.model.EPintura;

public class MainActivity extends AppCompatActivity {
    private CheckBox cpAr, cpCambio, cpAlarme, cpSolar, cpMultimidia, cpImportado;
    private RadioButton rb10, rb12, rb14, rb16, rb18, rb20;
    private Button btSalvar;
    private Spinner pintura;
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding();

        preencheSpinner();
        btSalvar.setOnClickListener(salvarClick());
    }

    private View.OnClickListener salvarClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double valorIPI = 0;
                double custo = 0;
                double taxaImportacao = 0;

                String temAr = "";
                String temCambio = "";
                String temAlarme = "";
                String temSolar = "";
                String temMult = "";
                String temPintura = "";
                String ehImportado = "";

                if (cpAr.isChecked()) {
                    custo = custo + 3000;
                    temAr = "Ar, ";
                }
                if (cpCambio.isChecked()) {
                    custo = custo + 5000;
                    temCambio = "Cambio automatico, ";
                }
                if (cpAlarme.isChecked()) {
                    custo = custo + 800;
                    temAlarme = "Alarme, ";
                }
                if (cpSolar.isChecked()) {
                    custo = custo + 4000;
                    temSolar = "Solar, ";
                }
                if (cpMultimidia.isChecked()) {
                    custo = custo + 1800;
                    temMult = "Kit Multimidia, ";
                }
                if (pintura.getSelectedItem() == Nenhuma){
                    custo = custo;
                } else if (pintura.getSelectedItem() != Nenhuma) {
                    custo = custo + 2500;
                    temPintura = "Pintura editada, ";
                }


                if (rb10.isChecked()) {
                    valorIPI = custo * 10 / 100;
                    custo = custo + valorIPI;
                } else {
                    valorIPI = custo * 20 / 100;
                    custo = custo + valorIPI;
                }
                if (cpImportado.isChecked()) {
                    taxaImportacao = custo * 30 / 100;
                    custo = custo + taxaImportacao;

                    ehImportado = "É importado ";
                }


                showToast("O custo do carro é de: R$"+custo+
                        "\n"+"Ele tem os acessórios: "+temAr+temCambio+temAlarme+temMult+temPintura+temSolar+ehImportado
                );
            }
        };
    }

    private void preencheSpinner() {
        pintura.setAdapter(new ArrayAdapter<EPintura>(this, android.R.layout.simple_spinner_item, EPintura.values()));
    }

    private void binding() {
        cpAr = findViewById(R.id.cbAr);
        cpCambio = findViewById(R.id.cbCambio);
        cpAlarme = findViewById(R.id.cbAlarme);
        cpSolar = findViewById(R.id.cbSolar);
        cpMultimidia = findViewById(R.id.cbMultimidia);
        cpImportado = findViewById(R.id.cbImportado);
        rb10 = findViewById(R.id.r1_0);
        rb12 = findViewById(R.id.r1_2);
        rb14 = findViewById(R.id.r1_4);
        rb16 = findViewById(R.id.r1_6);
        rb18 = findViewById(R.id.r1_8);
        rb20 = findViewById(R.id.r2_0);
        pintura = findViewById(R.id.spPintura);
        btSalvar = findViewById(R.id.button);

    }

    public void showToast(String message) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }
}