package com.example.lista5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.lista5.model.Peixe;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    Button btSalvar, btMostrar;
    TextInputLayout cpPeso;
    int sum = 0;
    Toast toast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        binding();

        btSalvar.setOnClickListener( salvarClick() );
        btMostrar.setOnClickListener( mostrarClick() );
    }

    private View.OnClickListener mostrarClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showToast("Multa total de R$"+sum);
            }
        };
    }

    private View.OnClickListener salvarClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Peixe p = new Peixe( Integer.parseInt(cpPeso.getEditText().getText().toString()) );

                if (p.getPeso() > 50.0){
                    int x = (int) p.getPeso();
                    int excedente = x - 50;
                    int multa = excedente * 4;
                    sum = sum + multa;

                    showToast("Multa de R$"+multa);
                } else {
                    showToast("Salvo");
                }
            }
        };
    }

    private void binding() {
        cpPeso = findViewById(R.id.tilPeso);
        btSalvar = findViewById(R.id.btnSalvar);
        btMostrar = findViewById(R.id.btnMostrar);
    }

    public void showToast(String message) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }
}