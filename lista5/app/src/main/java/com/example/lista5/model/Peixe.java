package com.example.lista5.model;

public class Peixe {
    private double peso;

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public Peixe() {
    }

    public Peixe(double peso) {
        this.peso = peso;
    }
}
